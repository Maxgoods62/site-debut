<?php
include_once('includes/token.class.php');
$token = Token::generer('inscription')
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Un formulaire de connexion en AJAX</title>
</head>

<?php
if (isset($_SESSION['id']) && isset($_SESSION['pseudo']))
       { ?>
        <p>Vous etes déja connecter !</p>
        Voulez vous <a href="deconnexion.php">vous déconnecter</a> ?
<?php  }
else
{
?>
<body>
<div id="formulaire"> 
<form name="inscriptionForm" id="inscriptionForm" action="inscription_conf.php" method = "post">
                <p>
                    <label for = "pseudo"><strong>Votre pseudo :</strong></label>
                    <input type = "text" name = "pseudo" id = "pseudo"/><br/>
                    <label for = "pass"><strong>Mot De Passe :</strong></label>
                    <input type ="text" name = "pass" id = "pass" required placeholder="Votre mot de passe"/><br/>
                    <label for = "confpass"><strong>Confirmation Du Mot De Passe :</strong></label>
                    <input type ="text" name = "confpass" id = "confpass" required placeholder="Retapez votre mot de passe"/><br/>
                    <label for = "email"><strong>Email:</strong></label>
                    <input type ="text" name = "email" id = "email"/><br/>
                    <div id="captchadiv">
                    <p><img src="captcha.php" width="200" height="50" alt="Code de vérification" id="captchaimg"/></p>
                    <small><a href="#" onclick=" document.getElementById('captchaimg').src = 'captcha.php?' + Math.random(); document.getElementById('captcha').value = ''; return false; ">refresh</a></small></p>
                    </div>
                    <p><label>Merci de retaper le code de l'image ci-dessus</label> : <input type="text" id= "captcha" name="captcha"/></p>
                    <input type="hidden" name="token" id="token" value="<?php echo $token;?>"/>
                    <input type = "submit" value = "Envoyer"/>
                </p>
</form>
</div>
<div id="resultat">
    <!-- Nous allons afficher un retour en jQuery au visiteur -->
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="js/inscription.js"></script>
<?php
}
include('footer.php');
?>
</body>
</html>
