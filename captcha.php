<?php
    /***************************************/
    /*** Captcha par bilou89 ***/
    /*** bilou89-4sang@hotmail.fr ***/
    /*** Modifié par Maxgoods ***/
    /*** Maxgoods remercie Bilou89 d'avoir créer ce code ! ***/
    /***************************************/
    if(!isset($_SESSION)){
              session_start();
            }  // Création de la session
    $largeur = 350; // Largeur de l'image
    $hauteur = 75; // Hauteur de l'image
    // Liste des polices utilisées aléatoirement, à placer dans le dossier police/
    $polices = array('polices/trebuc.ttf', 'polices/tahomabd.ttf', 'polices/verdanab.ttf');
    $image = imagecreatetruecolor($largeur, $hauteur); // Création de l'image
    $fond = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur de fond
    imagefill($image, 0, 0, $fond); // Coloration du fond
    /**************************************/
    /***  Traçage des lignes/cercles sur le fond ***/
    /**************************************/
    // 3 lignes vertial
    for($i = 0; $i < 3; $i++)
    {
            $couleur_ligne = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur de la ligne
            imagesetthickness($image, mt_rand(1, 4)); // Changement de l'épaisseur de la ligne
            imageline($image, 0, mt_rand(0, $hauteur), $largeur, mt_rand(0, $hauteur), $couleur_ligne); // Traçage de la ligne
    }
    // 3 lignes horizontal
    for($i = 0; $i < 3; $i++)
    {
            $couleur_ligne = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur de la ligne
            imagesetthickness($image, mt_rand(1, 4)); // Changement de l'épaisseur de la ligne
            imageline($image, mt_rand(0, $largeur), 0, mt_rand(0, $largeur), $hauteur, $couleur_ligne); // Traçage de la ligne
    }
    // 3 cercles
    for($i = 0; $i < 3; $i++)
    {
            $couleur_ligne = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur du cercle
            imagesetthickness($image, mt_rand(1, 2)); // Changement de l'épaisseur du cercle
            imageellipse($image, mt_rand(10, 290), mt_rand(10, 40), mt_rand(5, 60), mt_rand(5, 60), $couleur_ligne); //  Traçage du cercle
    }
    imagesetthickness($image, 1); // Remise de l'épaisseur des lignes, cercles, ... à 1px
    /**************************************/
    /*** Traçage des caractères sur le fond ***/
    /**************************************/
    $couleur_lettres = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur des lettres de fond
    $nb_caracteres = 25; // Nombre de caractères citués au fond
    $lettres = 'abcdefghijklmnopqrstuvwxyz-+*/@$&éè'; // Caractères utilisés pour les lettres de fond
    $lettres_melange = strlen($lettres); // Mélange des caractèred
    $x = mt_rand(5, 325); // Définition aléatoire de la position de X
    $y = mt_rand(5, 60); // Définition aléatoire de la position de Y
    $i = 0;
    // Boucle pour déssiner les caractères de fond
    while($i < $nb_caracteres)
    {      
            $lettre_a_ajouter = $lettres[mt_rand(0, $lettres_melange - 1)]; // Selection de la lettre à ajouter
            $taille = mt_rand(15, 25); // Définition de la taille de la lettre
            $angle = mt_rand(0, 180); // Définition de l'angle de la lettre
            imagettftext($image, $taille, $angle, $x ,$y, $couleur_lettres, realpath($polices[array_rand($polices)]), $lettre_a_ajouter); // Traçage de la lettre
            $x = mt_rand(35, 325); // Changement de position de X
            $y = mt_rand(10, 60); // Changement de position de Y
            $i++; // Inscrémentation de 1
    }
    /**************************************/
    /*** Traçage et création du code ***/
    /**************************************/
    $nb_caracteres = 7; // Nombre de caractères
    $lettres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789'; // Caractères autorisés
    $lettres_melange = strlen($lettres); // Mélange des caractères
    $code = ''; // Création de $code
    $x = mt_rand(5, 25); // Définition aléatoire de X
    $y = ($hauteur / 2) + mt_rand(0, 10); // Definition de Y ~ au milieu de l'image
    $i = 0;
    // Boucle pour dessiner les lettres
    while($i < $nb_caracteres)
    {
            $lettre_a_ajouter = $lettres[mt_rand(0, $lettres_melange - 1)]; // Selection de la lettres
            $taille = mt_rand(25,35); // Définition de la taille
            $angle = mt_rand(-30, 25); // Définition de l'angle
            $code .=  $lettre_a_ajouter; // Ajout de la lettre dans $code
            $couleur_lettre = imagecolorallocate($image, mt_rand(150, 255), mt_rand(150, 255), mt_rand(150, 255)); // Selection de la couleur de la lettre
            imagettftext($image, $taille, $angle, $x, $y, $couleur_lettre, realpath($polices[array_rand($polices)]), $lettre_a_ajouter); // Traçage de la lettre
            $x += $taille + mt_rand(8, 24); // Changement de position de X
            $y = ($hauteur / 2) + mt_rand(10, 15); // Petit changement de position de Y
            $i++; // Incrémentation de 1
    }
    /**************************************/
    /***  Traçage des lignes/cercles sur le devant ***/
    /**************************************/
    // 2 lignes vertial
    for($i = 0; $i < 2; $i++)
    {
            $couleur_ligne = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur de la ligne
            imageline($image, 0, mt_rand(0, $hauteur), $largeur, mt_rand(0, $hauteur), $couleur_ligne); // Traçage de la ligne
    }
    // 2 lignes horizontal
    for($i = 0; $i < 2; $i++)
    {
            $couleur_ligne = imagecolorallocate($image, mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)); // Couleur de la ligne
            imageline($image, mt_rand(0, $largeur), 0, mt_rand(0, $largeur), $hauteur, $couleur_ligne); // Traçage de la ligne
    }
    $_SESSION['code'] = $code; // Enregistrement du $code dans la session
    header('Content-type: image/png'); // Déclaration de la création d'un image PNG
    imagepng($image); // Création de l'image
    ?>
