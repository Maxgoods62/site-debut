<?php

include("includes/identifiants.php");
include_once('includes/token.class.php');
include_once('includes/securite.class.php');

if(Token::verifier(600, 'connexion')) 
{
	$captcha = $_POST['captcha'];
	$captcha = Securite::html($captcha);
  	$captcha = strtoupper($captcha); //  On met en majuscule le code de l'utilisateur car le code est en majuscule et sinon c'est l'erreur !
  	if($captcha==$_SESSION['code']) 
	{ // Si le champ est égal au code généré par l'image
		if(!empty($_POST['pseudo']) AND !empty($_POST['pass']))
		{

			  $pass = Securite::bdd($_POST['pass']);
			$pseudo = Securite::bdd($_POST['pseudo']);
			$pass_hache = hash('sha256','maxdm62goods' . $pass); // !! changer le salt pour le site !!

			// Vérification des identifiants
			$req = $bdd->prepare('SELECT id FROM membres WHERE pseudo = :pseudo AND pass = :pass');
			$req->execute(array(
			    'pseudo' => $pseudo,
			    'pass' => $pass_hache));

			$resultat = $req->fetch();

			if (!$resultat)
			{
			    echo 'BADLP'; //Mauvais identifiant ou mot de passe
			}
			else
			{
			    if(!isset($_SESSION)){
	   			  session_start();
				}
			    $_SESSION['id'] = $resultat['id'];
			    $_SESSION['pseudo'] = $pseudo;
			    echo 'SUCCESS'; // Connecté !

			}
		}
	}
	else {
		echo 'BADC'; // Mauvais Captcha
	}
}
else {
    echo 'BADT'; //Mauvais Token
}

?>
