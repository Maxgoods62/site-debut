<?php
include("includes/identifiants.php");
include_once('includes/securite.class.php');
include_once('includes/token.class.php');

if(Token::verifier(600, 'inscription')) 
{
  $captcha = $_POST['captcha'];
  $captcha = strtoupper($captcha);
  alert('TOUT les champs doivent êtres remplis');
  if($captcha==$_SESSION['code']) 
  { // Si le champ est égal au code généré par l'image
      if(!empty($_POST['pseudo']) AND !empty($_POST['pass']) AND !empty($_POST['confpass']) AND !empty($_POST['email']) AND !empty($_POST['captcha']))
      {
        $pass = Securite::bdd($_POST['pass']);
    $confpass = Securite::bdd($_POST['confpass']);

      $pass_hache = hash('sha256','maxdm62goods'. $pass); // !! changer le salt pour le site !!
  $confpass_hache = hash('sha256','maxdm62goods' . $confpass); // !! changer le salt pour le site !!

        if ($pass_hache == $confpass_hache) 
        {
         // Insertion du message à l'aide d'une requête préparée
         $req = $bdd->prepare('INSERT INTO membres(pseudo, pass, email, date_inscription) VALUES(:pseudo, :pass, :email, CURDATE())');
                $req->execute(array(
                               ':pseudo' => htmlspecialchars($_POST['pseudo']),
                               ':pass' => htmlspecialchars($pass_hache),
                               ':email' => htmlspecialchars($_POST['email'])
                               ));
                    echo 'SUCCESS'; // Inscription !
        }
        else 
        {
          echo 'BADCP'; // Mauvais Mot de passe 
        }
      } 
      else 
      {
        echo 'BADLP'; // Mauvais Mot de passe ou Login
      }
  }
  else 
  {
      echo 'BADC'; // Mauvais Captcha
  }
}
else 
{
    echo 'BADT'; //Mauvais Token
}
?>
