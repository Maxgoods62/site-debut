$(document).ready(function() {
    // Lorsque je soumets le formulaire
    $('#inscriptionForm').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
        var $this = $(this); // L'objet jQuery du formulaire
 
        // Je récupère les valeurs
        var pseudo = $('#pseudo').val();
        var pass = $('#pass').val();
        var confpass = $('#confpass').val();
        var captcha = $('#captcha').val();
        var token = $('#token').val();

        // Je vérifie une première fois pour ne pas lancer la requête HTTP
        // si je sais que mon PHP renverra une erreur
        if(pseudo === '' || pass === ''|| confpass === '' || captcha === '' || token === '') 
        {
            alert('TOUT les champs doivent êtres remplis');
        } 
        else 
        {
            // Envoi de la requête HTTP en mode asynchrone
            $.ajax(
            {
                url: $this.attr('action'), // Le nom du fichier indiqué dans le formulaire
                type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
                data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
                success: function(msg)
                {
                if(msg=='SUCCESS') // si la connexion en php a fonctionnée
                {
                    $("div#resultat").html("<span id=\"confirmMsg\">Vous &ecirc;tes maintenant inscrit.</span> Veuillez vous <a href='connexion.php'>connecter</a>.");
                    $("#inscriptionForm").hide();
                }
                else if(msg=='BADLP') // si mauvais mot de pass ou identifiant
                {
                    $("div#resultat").html("&nbsp;Erreur lors de l'inscription, veuillez v&eacute;rifier votre login et votre mot de passe.");
                    document.getElementById('captchaimg').src = 'captcha.php?' + Math.random(); document.getElementById('captcha').value = '';
                }
                else if(msg=='BADCP') // si mauvais captcha
                {
                    $("div#resultat").html("&nbsp;Erreur lors de l'inscription, veuillez v&eacute;rifier que la confirmation du mot de passe soit identique au mot de passe.");
                    document.getElementById('captchaimg').src = 'captcha.php?' + Math.random(); document.getElementById('captcha').value = '';
                }
                else if(msg=='BADC') // si mauvais captcha
                {
                    $("div#resultat").html("&nbsp;Erreur lors de l'inscription, veuillez v&eacute;rifier le captcha.");
                    document.getElementById('captchaimg').src = 'captcha.php?' + Math.random(); document.getElementById('captcha').value = '';
                }
                else if(msg=='BADT') // si mauvais token
                {
                    $("div#resultat").html("&nbsp;Erreur lors de l'inscription, veuillez recharger la page.");
                    document.getElementById('captchaimg').src = 'captcha.php?' + Math.random(); document.getElementById('captcha').value = '';
                }
                else  // si la connexion en php n'a pas fonctionnée
                {
                    $("div#resultat").html("&nbsp;Erreur de l'inscription.Problème de connexion a la base de donnée ? Si cela se réitère , veuillez en informer l'administrateur");
                    document.getElementById('captchaimg').src = 'captcha.php?' + Math.random(); document.getElementById('captcha').value = '';
                }
                }
            });
        }
    });
});